<?php
/*******************************
 * Template: Technical Requirements Page
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>
<?php get_header(); ?>

<?php include("inc/hasform.php");?>

<main class="homepage">
    <!--Homepage Hero Section-->
    <div class="technical_hero">
        <div class="container liquidity_content text-white">
            <div class="row pb">
                <div class="col-sm-6 align-self-start">
                    <h1 id="hero_heading">TECHNICAL REQUIREMENTS</h1>
                    <p>INFLYX’s FIX/API solutions represent the latest iteration of financial connectivity. You connect your own frontend to INFLYX’s exceptionally fast execution with APIs.</p>
                    <p>A variety of options exist for whichever platform you prefer to use. Our clients can connect with INFLYX’s liquidity either directly or by using any of the major MT4 bridges and software providers.</p>
                </div>
            </div>
            <div class="row technical_columns">
                <div class="col-12 col-md-5 col-lg-4 m-5 mx-auto p-4">
                    <h3>RELIABILITY</h3>
                    <p>INFLYX’s servers have a track record of consistency and reliability.</p>
                    <p>Multiple backups and power sources help to ensure that the servers are never down, and a number of tried and tested security systems and procedures prevent unauthorised access. </p>
                    <!-- Separator -->
                    <div class="container-fluid separator green_gray"></div>
                </div>
                <div class="col-12 col-md-5 col-lg-4 m-5 mx-auto p-4">
                    <h3>Fix Solutions</h3>
                    <p>The Financial Information eXchange protocol is the leading electronic communication protocol for transactions in financial securities.</p>
                    <p>The longstanding experience of INFLYX’s staff in using FIX helps us minimise latency for our clients and enhance the functionality of your frontent GUI.</p>
                    <!-- Separator -->
                    <div class="container-fluid separator green_gray"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Separator -->
    <div class="container-fluid separator"></div>

    <!-- Why Inflyx -->
    <?php include("inc/why_inflyx.php"); ?>

    <!-- Separator -->
    <div class="container-fluid separator2"></div>

    <div class="container-fluid pt-5 bg_dark_green contact_sec">
        <div class="container homepage_talk pt-5">
            <div class="row pt-5">
                <div class="col-md-6 homepage_talk_to p-5">
                    <img alt="Inflyx" class="pt-5" src="<?php echo get_template_directory_uri(); ?>/assets/img/inflyx-logo.svg">
                    <h3 class="py-4">We would love to talk to you</h3>
                    <p>Feel free to contact us.</p>

                    <!-- Separator -->
                    <div class="container-fluid separator green_gray"></div>
                </div>

                <div class="col-md-6 text-white pl-5">
                    <?php include("inc/contactform.php"); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Separator -->
    <div class="container-fluid separator"></div>

    <!-- Ass seen on section -->
    <?php include("inc/as_seen_on.php"); ?>

</main>

<?php get_footer(); ?>