<?php
/*******************************
 * Template: Header
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>
 
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="UTF-8">
    <title><?php echo bloginfo('name');?> | Page not found 404</title>
    <script src="<?php echo get_theme_file_uri(); ?>/assets/js/jquery.js"></script>
    <!-- custom theme scripts -->
    <?php include("inc/gsap.php"); ?>

    <?php wp_head();?>
</head>

<body>

<style>
    html, body {
        background: rgba(10, 106, 121, 0.9);
        overflow: hidden;
    }
    a {
        color: #fff;
    }
    a:hover{
        color: #c7c7c7;
    }

</style>

<main class="_404">

    <!--Homepage Hero Section-->
    <div class="_404_hero">
        <div class="_404_hero_video"></div>
    </div>

    <div class="_404_content">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
            <img alt="Inflyx" class="box" id="page_logo" src="<?php echo get_theme_file_uri(); ?>/assets/img/inflyx-logo.svg">
        </a>
        <span class="coordinates"></span>
        <p class="_404_text py-5 box">
            4<img alt="404 page" src="<?php echo get_theme_file_uri(); ?>/assets/img/lifebuoy.svg">4
        </p>
        <h4 class="py-3 box">It looks like you're in deep waters.</h4>
        <h4 class="py-2 box">Here's a lifebuoy.</h4>
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><h3 class="box">Save me!</h3></a>
    </div>


</main>

<script>
    $(document).ready(function() {
        if($(window).width()>767) {
            $video = '<video playsinline muted loop poster="<?php echo get_theme_file_uri(); ?>/assets/img/heroScreenshot.webp" id="bgvid"> <source src="<?php echo get_theme_file_uri(); ?>/assets/video/shark.mp4" type="video/mp4"></video>';
            
            $('._404_hero_video').html($video);
            $('#bgvid').get(0).play();
        }
        var box = $('.box');
        TweenLite.from(box, 6, {y:-700});
    });

    $(document).mousemove(function(event){
        var xPos = (event.clientX/$(window).width())-0.5,
            yPos = (event.clientY/$(window).height())-0.5,
            box = $('.box'),
            coord = $('.coordinates');
        TweenLite.set(box, {perspective:800});
        TweenLite.set(box, {transformStyle:"preserve-3d"﻿});
        TweenLite.to(box, 5.6, {
            rotationY: 55 * xPos,
            rotationX: 125 * yPos,
            y: -250 * yPos,
            x: -100 * yPos,
            ease: Power1.easeOut,
            transformPerspective: 900,
            transformOrigin: 'center'
        });
    });
</script>

