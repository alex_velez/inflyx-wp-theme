<?php
/*******************************
 * Template: Index - Blog Page
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>

<?php get_header(); ?>

<?php include("inc/hasform.php")?>

<main class="homepage">
    <!--Homepage Hero Section-->
    <div class="blog_hero">
        <div class="container liquidity_content text-white">
            <div class="row">
                <div class="col-sm-6 align-self-start">
                    <h1 id="hero_heading">Blog</h1>
                </div>
            </div>
            <div class="row blog_columns">
               
               <?php
                //get the current page
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                //pagination fixes prior to loop
                $temp =  $query;
                $query = null;

                //custom loop using WP_Query
                $query = new WP_Query( array(
                    'post_status' => 'publish',
                    'orderby' => 'date',
                    'order' => 'ASC'
                ) );

                //set our query's pagination to $paged
                $query -> query('post_type=post&posts_per_page=6' .'&paged='.$paged);
                $max_pages = $query->max_num_pages;
                if ( $query->have_posts() ) :
                while ( $query->have_posts() ) : $query->the_post();
                    ?>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 m-3">
                        <article class="archive-page-article">
                            <a href="<?php the_permalink(); ?>">
                                <?php 
                                    if (has_post_thumbnail()) {
                                        the_post_thumbnail('full' , array( 'class' => 'img-fluid featured-img' ) );
                                    } else { ?>
                                        <img src="<?php echo get_theme_file_uri(); ?>/assets/img/nothumb.png" alt="Inflyx Solutions" class="img-fluid featured-img" >
                                <?php }
                                ?>
                            </a>
                            <div class="blog_info p-4">
                                <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
                                <div class="blog_exceprt">
                                    <?php the_excerpt('2'); ?>
                                </div>
                                <span><a href="<?php the_permalink(); ?>" class="blog_read pb-5">Read more</a></span><span class="blog_date"><?php echo get_the_date(); ?></span>
                            </div>
                            <!-- Separator -->
                            <div class="container-fluid separator green_gray"></div>

                        </article>
                    </div>

                <?php endwhile;?>
            </div>
            <div class="row mt-6">
                <div class="col-12 text-center mx-auto pagination_pad">
                    <nav class="text-center mx-auto">
                        <ul class="pagination text-center mx-auto">
                            <?php pagination_bar($max_pages); ?>
                        </ul>
                    </nav>
                </div>
            </div>
            <?php else : ?>
                <div class="nopost text-white">No Posts Found.</div>
            <?php endif; ?>
            <?php //reset the following that was set above prior to loop
            $query = null; $query = $temp; ?>

            </div>

        </div>

    </div>

    <!-- Separator -->
    <div class="container-fluid separator"></div>

    <!-- Why Inflyx -->
    <?php include("inc/why_inflyx.php"); ?>

    <!-- Separator -->
    <div class="container-fluid separator2">

    </div>
    <div class="container-fluid pt-5 bg_dark_green contact_sec">
        <div class="container homepage_talk pt-5">
            <div class="row pt-5">
                <div class="col-md-6 homepage_talk_to p-5">
                    <img alt="Inflyx Liquidity" class="pt-5" src="<?php echo get_theme_file_uri(); ?>/assets/img/inflyx-logo.svg">
                    <h3 class="py-4">We would love to talk to you</h3>
                    <p>Feel free to contact us.</p>

                    <!-- Separator -->
                    <div class="container-fluid separator green_gray"></div>
                </div>

                <div class="col-md-6 text-white pl-5">
                    <?php include("inc/contactform.php"); ?>
                </div>
            </div>
        </div>

    </div>

    <!-- Separator -->
    <div class="container-fluid separator"></div>

    <!-- As seen on -->
    <?php include("inc/as_seen_on.php"); ?>
    
</main>

<?php get_footer(); ?>
