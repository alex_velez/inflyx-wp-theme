<?php
/*******************************
 * Template: Onboarding Page
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>
<?php get_header(); ?>

<?php include("inc/hasform.php"); ?>
<?php include("inc/gsap.php");?>
    
<main class="homepage">
    <!--Homepage Hero Section-->
    <div class="onboarding_hero">
        <div class="container liquidity_content text-white">
            <div class="row pb">
                <div class="col-md-6">
                    <h1 class="pb-4" id="hero_heading">Effortless INTEGRATION - ONBOARDING</h1>
                    <p id="hero_para" class="pb-4">As a leading liquidity provider, INFLYX offers the highest level of service when onboarding clients. Integrate our liquidity and risk management services with a simple phone call and a few straightforward steps to.</p>
                    <ol>
                        <li>Complete the form</li>
                        <li>Talk to one of our account managers about your requirements</li>
                        <li>Provide the documents required - in most cases, we can do this for you and you only need to check and confirm the details</li>
                    </ol>

                    <p class="pt-5">
                       <strong>That's it: you can be up and running with INFLYX in the next to no time. If you need more information about any aspect of our services or if you'd like to request a trial, please contact us by completing and sending the form.</strong>
                    </p>

                </div>
                <div class="col-md-6">
                    <h5 class="pt-5">Fill the form</h5>
                    <form name="contactForm" class="ajaxForm2" id="contactForm2" action="https://formcarry.com/s/L2ny9cQvOKY" method="POST" accept-charset="UTF-8" >
                        <input type="text" name="fullName" id="firstName2" placeholder="Name">
                        <p style="color: #E4252F; font-size: 13px;" id="name-error2"></p>
                        <input type="text" name="phone" placeholder="Phone Number">
                        <input type="email" name="email" id="emailAddress2" placeholder="Email">
                        <p style="color: #E4252F; font-size: 13px;" id="email-error2"></p>
                        <input type="hidden" name="_gotcha">
                        <input type="submit" value="Submit">
                    </form>

                </div>
            </div>

        </div>

    </div>
    <!-- Separator -->
    <div class="container-fluid separator">

    </div>
    <div class="container-fluid bg_dark_green bg_linear py-5">
        <div class="row text-white text-center py-5">
            <div class="col-12 text-center d-md-none">
                <h5 class="pb-5">DISTRIBUTION OF LIQUIDITY</h5>
                <img class="img-fluid" src="<?php echo get_theme_file_uri(); ?>/assets/img/distribution_liq_mbl.svg" alt="Distribution of Liquidity">
            </div>
            <div class="col-12 text-center d-none d-md-block">
                <h5 class="pb-5">DISTRIBUTION OF LIQUIDITY</h5>
                <img class="img-fluid" src="<?php echo get_theme_file_uri(); ?>/assets/img/distribution_liq.svg" alt="Distribution of Liquidity">
            </div>
        </div>
    </div>


    <!-- Why Inflyx -->
    <?php include("inc/why_inflyx.php"); ?>

    <!-- Separator -->
    <div class="container-fluid separator2"></div>

    <div class="container-fluid pt-5 bg_dark_green contact_sec">
        <div class="container homepage_talk pt-5">
            <div class="row pt-5">
                <div class="col-md-6 homepage_talk_to p-5">
                    <img alt="Inflyx" class="pt-5" src="<?php echo get_theme_file_uri(); ?>/assets/img/inflyx-logo.svg">
                    <h3 class="py-4">We would love to talk to you</h3>
                    <p>Feel free to contact us.</p>

                    <!-- Separator -->
                    <div class="container-fluid separator green_gray"></div>
                </div>

                <div class="col-md-6 text-white pl-5">
                    <?php include("inc/contactform.php"); ?>
                </div>
            </div>
        </div>

    </div>

    <!-- Separator -->
    <div class="container-fluid separator"></div>

    <!-- Ass seen on section -->
    <?php include("inc/as_seen_on.php"); ?>

</main>

<script>
    $(document).ready(function() {
        TweenLite.from(".liquidity_content", 1.5,{opacity: 0, delay: 0.8});
    });
</script>


<?php get_footer(); ?>