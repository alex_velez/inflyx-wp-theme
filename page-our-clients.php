<?php
/*******************************
 * Template: Our Clients Page
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>
<?php get_header(); ?>

<?php include("inc/hasform.php");?>
<?php include("inc/gsap.php");?>

<!--Homepage Hero Section-->
<div class="liquidity_hero">
        <div class="container liquidity_content text-white">
            <div class="row pb">
                <div class="col-12 col-md-6 align-self-start">
                    <h2 id="hero_heading">Liquidity Services</h2>
                    <p id="hero_para">Expand your options with incomparable market depth and access to a vast array of symbols</p>
                </div>
            </div>
            <div class="row pt-5">
                <div class="col-md-6 d-none d-md-block">
                    <ul class="bullets">
                        <li id="bull_1a" class="bull_1a activelist"><span class="bull_1a">Brokers/White Labels</span></li>
                        <li id="bull_2a" class="bull_2a"><span class="bull_2a">Hedge Funds</span></li>
                        <li id="bull_3a" class="bull_3a"><span class="bull_3a">Investment Firms</span></li>
                        <li id="bull_4a" class="bull_4a"><span class="bull_4a">Asset Managers</span></li>
                        <li id="bull_5a" class="bull_5a"><span class="bull_5a">Professional Traders</span></li>
                        <li id="bull_6a" class="bull_6a"><span class="bull_6a">Proprietary Trading Houses</span></li>
                        <li id="bull_7a" class="bull_7a"><span class="bull_7a">Private Banks/ Corporates</span></li>
                    </ul>
                </div>
                <div class="col-sm-6 bullets_content">
                    <h4 id="bull_1">Brokers / White Labels</h4>
                    <p class="mouse_scroll bull_1">
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem
                        quia voluptas sit aspernatur aut odit aut fugit, sed quia. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur,
                        adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam.
                    </p>
                    <h4 id="bull_2">Hedge Funds</h4>
                    <p class="mouse_scroll bull_2">
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem
                        quia voluptas sit aspernatur aut odit aut fugit, sed quia. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur,
                        adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam.
                    </p>
                    <h4 id="bull_3">Investment Firms</h4>
                    <p class="mouse_scroll bull_3">
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem
                        quia voluptas sit aspernatur aut odit aut fugit, sed quia. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur,
                        adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam.
                    </p>
                    <h4 id="bull_4">Asset Managers</h4>
                    <p class="mouse_scroll bull_4">
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem
                        quia voluptas sit aspernatur aut odit aut fugit, sed quia. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur,
                        adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam.
                    </p>
                    <h4 id="bull_5">Professional Traders</h4>
                    <p class="mouse_scroll bull_5">
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem
                        quia voluptas sit aspernatur aut odit aut fugit, sed quia. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur,
                        adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam.
                    </p>
                    <h4 id="bull_6">Proprietary Trading Houses</h4>
                    <p class="mouse_scroll bull_6">
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem
                        quia voluptas sit aspernatur aut odit aut fugit, sed quia. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur,
                        adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam.
                    </p>
                    <h4 id="bull_7">Private Banks/Corporates</h4>
                    <p class="mouse_scroll bull_7">
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem
                        quia voluptas sit aspernatur aut odit aut fugit, sed quia. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur,
                        adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam.
                    </p>
                </div>

            </div>

        </div>

    </div>

    <!-- Separator -->
    <div class="container-fluid separator"></div>

    <!-- Why Inflyx -->
    <?php include("inc/why_inflyx.php"); ?>

    <!-- Separator -->
    <div class="container-fluid separator2"></div>

    <div class="container-fluid pt-5 bg_dark_green contact_sec">
        <div class="container homepage_talk pt-5">
            <div class="row pt-5">
                <div class="col-md-6 homepage_talk_to p-5">
                    <img alt="Inflyx" class="pt-5" src="<?php echo get_theme_file_uri(); ?>/assets/img/inflyx-logo.svg">
                    <h3 class="py-4">We would love to talk to you</h3>
                    <p>Feel free to contact us.</p>

                    <!-- Separator -->
                    <div class="container-fluid separator green_gray"></div>
                </div>

                <div class="col-md-6 text-white pl-5">

                    <?php include("inc/contactform.php"); ?>

                </div>
            </div>
        </div>

    </div>
    <!-- Separator -->
    <div class="container-fluid separator"></div>
    
    <!-- As Seen on -->
    <?php include("inc/as_seen_on.php"); ?>

</main>

<script>
    $(document).ready(function() {
        $("ul.bullets li").click(function () {
            var current = "#" + event.target.classList[0].toString();

            var curr_content = current.substring(0, current.length-1);

            TweenLite.to(".bullets_content", 1, {scrollTo:{y: curr_content, offsetY:70, autoKill:false}});
            $("li").removeClass('activelist');
            $(current).addClass("activelist");
        })
        $(".mouse_scroll").hover(function () {
            var current_para = "#" + event.target.classList[1].toString() + "a";
            $("li").removeClass('activelist');
            $(current_para).addClass("activelist");
        });
    });
    </script>

<?php get_footer(); ?>