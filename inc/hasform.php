<?php
/*******************************
 * Template: Theme Scripts
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>
 
<!-- FORM Scripts -->
<script>
	$(function(){
		$(".ajaxForm").submit(function(e){
			e.preventDefault();
			if($("#firstName").val().length == 0) {
				$("#name-error").text("Please enter your name");
				return false;
			}else if ($("#emailAddress").val().length == 0){
				$("#name-error").text("");
				$("#last-name-error").text("");
				$("#email-error").text("Please enter your email");
				return false;
			}else {
				var href = $(this).attr("action");
				$.ajax({
					type: "POST",
					dataType: "json",
					url: href,
					data: $(this).serialize(),
					success: function(response){
						if(response.status == "success"){
							window.location.href = "https://inflyx.com/thank-you/";
						}else{
							alert("An error occured: " + response.message);
						}
					}
				});
			}
		});
	});
</script>

<script>
    $(function(){
        $(".ajaxForm2").submit(function(e){
            e.preventDefault();
            if($("#firstName2").val().length == 0) {
                $("#name-error2").text("Please enter your name");
                return false;
            }else if ($("#emailAddress").val().length == 0){
                $("#name-error2").text("");
                $("#last-name-error2").text("");
                $("#email-error2").text("Please enter your email");
                return false;
            }else {
                var href = $(this).attr("action");
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: href,
                    data: $(this).serialize(),
                    success: function(response){
                        if(response.status == "success"){
                            window.location.href = "https://inflyx.com/thank-you/";
                        }else{
                            alert("An error occured: " + response.message);
                        }
                    }
                });
            }
        });
    });
</script>