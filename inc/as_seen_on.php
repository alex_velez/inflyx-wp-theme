<div class="container-fluid bg_gray py-5 as_seen_on">
    <div class="container py-5">
        <div class="row">
            <div class="col-12">
                <h3>As seen on:</h3>
                <img src="<?php echo get_theme_file_uri(); ?>/assets/img/finance-magnates.webp" alt="Finance Magnates">
                <img src="<?php echo get_theme_file_uri(); ?>/assets/img/fxstreet-logo.webp" alt="FXSTREET">
                <img src="<?php echo get_theme_file_uri(); ?>/assets/img/leaprate-logo.webp" alt="LeapRate.com - Online Trading News">   
            </div>
        </div>
    </div>
</div>