<?php
/*******************************
 * Template: include / Our Team
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>

<div class="container-fluid bg_gray our-team-header">
    <div class="container">
        <div class="row section_why2 pb-5">
            <div class="col-12">
                <h4>Our Team</h4>
                <p>INFLYX's management team draws together a diverse range of people with decades of collective experience in liquidity and risk management</p>
                <!-- Separator -->
                <div class="container-fluid separator3 w-25"></div>
            </div>
        </div>

        <div class="row our_team">
            <div class="col-12 col-sm-6 col-md-3">
                <img src="<?php echo get_theme_file_uri(); ?>/assets/img/team/marios-antoniou.webp" alt="Marios Antoniou">
                <h5 class="pt-4">Marios Antoniou</h5>
                <h6 class="pb-4">EXECUTIVE DIRECTOR & CHIEF RISK OFFICER</h6>
                <p>An energetic and driven person, Marios uses the skills learnt as a senior dealer and head of dealing on own account at major brokers and risk manager at a major bank to enhance INFLYX’s offerings.</p>
                <p>He is passionate about transparency in liquidity.</p>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <img src="<?php echo get_theme_file_uri(); ?>/assets/img/team/avgoustinos-athiainitis.webp" alt="Avgoustos Athiainitis">
                <h5 class="pt-4">Avgoustinos Athianitis</h5>
                <h6 class="pb-4">HEAD OF ACCOUNTING & FINANCE</h6>
                <p>Having developed a firm grounding in the key principles of wealth management and auditing at a major consulting company, Avgoustinos brings experience as well as acumen to INFLYX’s management team. </p>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <a href="<?php echo esc_url(site_url("/paul-orford/")); ?>">
                    <img src="<?php echo get_theme_file_uri(); ?>/assets/img/team/paul-orford.webp" alt="Paul Orford">
                </a>
                <h5 class="pt-4">Paul Orford</h5>
                <h6 class="pb-4">HEAD OF INSTITUTIONAL SALES</h6>
                <p>Operating in the financial markets for the past 15 years and a part of the institutional space for the last 6,
                    Paul brings his expertise and unique blend of skills to our institutional sales department.</p>
                <p>He is the owner of Game Changers magazine aimed at B2B FX and a regular contributor to Finance Feeds.</p>
            </div>
        </div>
    </div>
</div>
