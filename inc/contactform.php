<?php
/*******************************
 * Template: Contact Form
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>
<form name="contactForm" class="ajaxForm generic-form" id="contactForm" action="https://formcarry.com/s/L2ny9cQvOKY" method="POST" accept-charset="UTF-8" >
    <input type="text" name="fullName" id="firstName" placeholder="Name">
    <p class="show-error" id="name-error"></p>
    <input type="text" name="phone" placeholder="Phone Number">
    <input type="email" name="email" id="emailAddress" placeholder="Email">
    <p class="show-error" id="email-error"></p>
    <input type="hidden" name="_gotcha">
    <textarea name="message" placeholder="Type your message here..." rows="2" class="col-12"></textarea>
    <input type="submit" value="Submit">
</form>