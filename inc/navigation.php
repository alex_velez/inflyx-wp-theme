<?php
/*******************************
 * Template: Header
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>
 
 <script>
    $( document ).ready(function() {
        $(".menu-trigger").click( function(e) {e.preventDefault();
        openNav();
        return false; } );
    });
</script>

 <div class="logo_container">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <a href="<?php echo esc_url( site_url('/') ); ?>">
                    <img id="page_logo" src="<?php echo get_theme_file_uri(); ?>/assets/img/inflyx-logo.svg" alt="Inflyx">
                </a>
            </div>
            <div class="col-6 text-right">
                <a class="menu-trigger">
                    <span class="line line-1"></span>
                    <span class="line line-3"></span>
                </a>
                <div id="myNav" class="overlay">
                    <div class="container">
                        <div class="row">
                            <div class="col-6 text-left inner_inflyx_logo">
                                <a href="<?php echo esc_url( site_url('/') ); ?>">
                                    <img src="<?php echo get_theme_file_uri(); ?>/assets/img/inflyx-logo.svg" alt="Inflyx">
                                </a>
                            </div>
                            <div class="col-6">
                                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                            </div>
                        </div>
                    </div>
                    <div class="overlay-content">
                        <a href="<?php echo esc_url( site_url('/liquidity/')); ?>">Liquidity</a>
                        <a href="<?php echo esc_url( site_url('/onboarding/')); ?>">Onboarding</a>
                        <a href="<?php echo esc_url( site_url('/technical-requirements/')); ?>">Technical Requirements</a>
                        <a href="<?php echo esc_url( site_url('/blog/')); ?>">Blog</a>
                        <a href="<?php echo esc_url( site_url('/about/')); ?>">About</a>
                        <a href="<?php echo esc_url( site_url('/contact/')); ?>">Contact</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $( document ).ready(function() {
       $(".menu-trigger").click( function(e) {e.preventDefault();
           openNav();
           return false; } );
    });
</script>