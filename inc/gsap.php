<?php
/*******************************
 * Template: Gsap Needs
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>

<!-- GSAP -->
<script src="<?php echo get_theme_file_uri(); ?>/assets/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo get_theme_file_uri();?>/assets/js/inflyx.min.js"></script>