<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
    <div class="tradingview-widget-container__widget"></div>
    <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/markets/currencies/" rel="noreferrer noopener" target="_blank"><span class="blue-text">Forex</span></a> by TradingView</div>
    <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-market-quotes.js" async>
        {
            "width": 770,
            "height": 450,
            "symbolsGroups": [
            {
                "originalName": "Forex",
                "symbols": [
                    {
                        "name": "FX:EURUSD"
                    },
                    {
                        "name": "FX:GBPUSD"
                    },
                    {
                        "name": "FX:USDJPY"
                    },
                    {
                        "name": "FX:USDCHF"
                    },
                    {
                        "name": "FX:AUDUSD"
                    },
                    {
                        "name": "FX:USDCAD"
                    }
                ],
                "name": "Forex"
            }
        ],
            "locale": "en"
        }
    </script>
</div>
<!-- TradingView Widget END -->