<?php
/*******************************
 * Template: include / Why Inflyx
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>

<div class="container-fluid bg_gray">
    <div class="container">
        <div class="row section_why3 sec_why_pad">
            <div class="col-12 col-md-4 pb-5">
                <h4>Why Inflyx?</h4>
                <p>Because we can give you a competitive advantage over other businesses.</p>
                <!-- Separator -->
                <div class="container-fluid separator3 w-25"></div>
            </div>
            <div class="col-6 col-md-2">
                <img src="<?php echo get_theme_file_uri(); ?>/assets/img/flexibility.webp" alt="Flexibility">
                <h5>Flexibility</h5>
            </div>
            <div class="col-6 col-md-2">
                <img src="<?php echo get_theme_file_uri(); ?>/assets/img/seamless-integration.webp" alt="Seamless Integration">
                <h5>Seamless <br> Integration</h5>
            </div>
            <div class="col-6 col-md-2">
                <img src="<?php echo get_theme_file_uri(); ?>/assets/img/interbank-spreads.webp" alt="Interbank Spreads">
                <h5>Interbank <br> Spreads</h5>
            </div>
            <div class="col-6 col-md-2">
                <img src="<?php echo get_theme_file_uri(); ?>/assets/img/dedicated-support.webp" alt="Dedicated Support">
                <h5>Dedicated <br> Support</h5>
            </div>
        </div>
    </div>
</div>