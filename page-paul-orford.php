<?php
/*******************************
 * Template: Paul Orford Page
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>

<?php get_header(); ?>

<?php include("inc/hasform.php")?>

<main class="homepage">
    <!--Homepage Hero Section-->
    <div class="team-member-page">
        <div class="container">
           
            <div class="row justify-content-around">

                <div class="col-12">
                    <div class="contact-title d-none d-md-block">
                        We would love to talk to you
                    </div>
                </div>

                <div class="col-md-6 contact-box">
                    <img alt="Paul Orford" src="<?php echo get_theme_file_uri(); ?>/assets/img/paul-orford.webp">
                    <div class="team-member-text">
                        <h2>Paul Orford</h2>
                        <h3>HEAD OF INSTITUTIONAL SALES</h3>
                        <p>Operating in the financial markets for the past 15 years and a part of the institutional space for the last 6, Paul brings his expertise and unique blend of skills to our institutional sales department. </p>
                        <p>He is the owner of Game Changers magazine aimed at B2B FX and a regular contributor to Finance Feeds.</p> 
                    </div>
                    <!-- Separator -->
                    <div class="container-fluid separator green_gray"></div>
                </div>

                <div class="col-12 col-md-5 text-white contact-form my-auto">
                    <h1>Contact Paul Orford</h1>
                    <form name="contactForm" class="ajaxForm" id="contactForm" action="https://formcarry.com/s/L2ny9cQvOKY" method="POST" accept-charset="UTF-8" >
                        <input type="text" name="fullName" id="firstName" placeholder="Name">
                        <p class="show-error" id="name-error"></p>
                        <input type="text" name="phone" placeholder="Phone Number">
                        <input type="email" name="email" id="emailAddress" placeholder="Email">
                        <p class="show-error" id="email-error"></p>
                        <input id="submitted" name="Submitted" type="hidden" value="on Paul Orford Page">
                        <input type="hidden" name="_gotcha">
                        <textarea name="message" placeholder="Type your message here..." rows="2" class="col-12"></textarea>
                        <input type="submit" value="Submit">
                    </form>
                </div>

            </div>

        </div>
    </div>

    <!-- Separator -->
    <div class="container-fluid separator"></div>


    <!-- Separator -->
    <div class="container-fluid separator"></div>

    <!-- As seen on -->
    <?php include("inc/as_seen_on.php"); ?>
    
</main>

<?php get_footer(); ?>
