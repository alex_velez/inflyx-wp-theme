<?php
/*******************************
 * Template: Thank You Page
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>

<?php get_header(); ?>

<main style="width:100%; height:100%; background-color:#0F8587;">

    <div class="container py-5">
        <div style="padding-top: 10vh;" class="row text-center">
            <div class="col-12 mx-auto text-center py-5">
                <img alt="Inflyx" src="<?php echo get_theme_file_uri(); ?>/assets/img/inflyx-logo.svg">
            </div>
        </div>
        <div class="row py-5">
           <div class="col-12 mx-auto text-center">
                <h1 class="text-white text-center py-4 display-1">THANK YOU</h1>
            </div> 
        </div>
        <div class="row py-5">
            <div class="col-12 mx-auto text-center">
                <h4 class="text-white text-center py-4">We received your message, <br>someone from our team will contact you shortly.</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12 mx-auto text-center pb-5">
                <a class="thank-you" href="<?php echo esc_url( site_url('/') ); ?>">Go back to Homepage</a>
            </div>
        </div>
    </div>

    <!-- Separator -->
    <div class="container-fluid separator"></div>

</main>

<?php get_footer(); ?>