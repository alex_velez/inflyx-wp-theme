<?php
/*******************************
 * Template: Single.php 
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>
<style>
    .logo_container {
        position:relative!important;
        top:0!important;
        padding:30px 0;
        background:rgba(10, 106, 121, 1);
    }
</style>

<?php get_header(); ?>

    <main class="main--single">

        <div class="container">
            <div class="row single_page">

                <div class="col-12">
                    <?php
                    while ( have_posts() ) : the_post(); ?>
                    <article class="blog-article">
                        <div class="py-4">
                            <h1><?php the_title();?></h1>
                        </div>
                        <?php
                                the_content();
                        endwhile; ?>
                    </article>
                </div>
                
            </div><!-- end row-->
        </div>
        
    </main>

<?php get_footer(); ?>