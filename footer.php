<?php
/*******************************
 * Template: Footer
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>

<!-- Open / Close Navigation -->
<script>
    function openNav() {
        document.getElementById("myNav").style.width = "100%";
    }
    function closeNav() {
        document.getElementById("myNav").style.width = "0%";
    }
</script>

<footer>
    <div class="container-fluid bg_dark_gray py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-7 col-lg-9">
                    <img src="<?php echo get_theme_file_uri(); ?>/assets/img/inflyx-logo-alt.webp" alt="INFLYX">
                    <ul class="footer_list">
                        <li><a href="<?php echo get_theme_file_uri(); ?>/assets/docs/order-execution-policy.pdf" target="_blank">Order Execution Policy</a></li>
                        <li><a href="<?php echo get_theme_file_uri(); ?>/assets/docs/conflict-of-interest.pdf" target="_blank">Conflicts of Interest Policy</a></li>
                        <li><a href="<?php echo get_theme_file_uri(); ?>/assets/docs/privacy-policy.pdf" target="_blank">Privacy Policy</a></li>
                        <li><a href="<?php echo get_theme_file_uri(); ?>/assets/docs/risk-disclosure-statement.pdf" target="_blank">Risk Disclosure Statement</a></li>
                        <li><a href="/legal-documents">Legal Documents</a></li>
                    </ul>
                    <p>INFLYX is a trade name of ICC Intercertus Capital Limited. ICC Intercertus Capital Limited is authorised and regulated by the Cyprus
                        Securities and Exchange Commission (CySEC) with licence number 301/16 and registration number HE 346662.</p>
                    <p>The information contained in this website is of a general nature only. The information does not constitute advice or a recommendation to act upon the information
                        or an offer and does not take into account your personal circumstances, financial situation or needs. You are strongly recommended
                        to seek professional advice before opening an account with us. Trading in FX and derivatives carries significant risks and it is not
                        suitable for all investors. You may incur a loss that is substantially greater than the amount you invested. Please read our legal
                        documents.</p>
                    <p>Inflyx does not offer contracts for difference to residents of certain jurisdictions such as the USA, Belgium, Iran, Canada
                        and North Korea This website is not directed at any person in countries or jurisdictions where the offer of such financial products is
                        not permitted or is unlawful.</p>
                </div>

                <div class="col-12 col-md-5 col-lg-3 footer_social_media">
                    <h5>Follow us on social media</h5>
                    <div class="py-3">
                        <span><a href="https://www.facebook.com/inflyx/" target="_blank" rel="noreferrer noopener">
                            <img src="<?php echo get_theme_file_uri(); ?>/assets/img/facebook.webp" alt="Facebook"></a></span>
                        <span><a href="https://www.linkedin.com/company/inflyx/" target="_blank" rel="noreferrer noopener">
                            <img src="<?php echo get_theme_file_uri(); ?>/assets/img/linkedin.webp" alt="Linkedin"></a></span>
                    </div>
                    <div class="py-3">
                        <h5>Give us a call</h5>
                        <p class="phonenumber">+ 357 25 885090</p>
                    </div>
                    <p class="py-3">
                        &copy; Inflyx 2020. All rights reserved.
                    </p>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Separator -->
    <div class="container-fluid separator"></div>

</footer>

    <?php wp_footer(); ?>
    </body>
</html>