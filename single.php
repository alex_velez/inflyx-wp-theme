<?php
/*******************************
 * Template: Single.php 
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>
<style>
    .logo_container {
        position:relative!important;
        top:0!important;
        padding:30px 0;
        background:rgba(10, 106, 121, 1);
    }
</style>

<?php get_header(); ?>

    <main class="main--single">

        <div class="container">
            <div class="row single_page">

                <div class="col-12 breadcrumb">
                    <ul>
                        <li><a href="<?php echo esc_url( site_url('/blog/')); ?>">Blog</a></li>
                        <li><?php the_title();?></li>
                    </ul>
                </div>

                <div class="col-12">
                    <?php
                    while ( have_posts() ) : the_post(); ?>

                    <article class="blog-article">
                        <p class="article-date"><?php the_date();?></p>
                        <?php
                            if (has_post_thumbnail()) { ?>
                                <figure>
                                    <?php $img_id = get_post_thumbnail_id(get_the_ID()); ?>
                                    <img alt="<?php echo get_post_meta( $img_id, '_wp_attachment_image_alt', true ); ?>" class="img-fluid" src="<?php echo get_the_post_thumbnail_url(); ?>">
                                </figure>
                            <?php }
                        ?>
                        
                        <h1><?php the_title();?></h1>

                        <?php the_content();?>

                    </article> <?php
                            
                    endwhile; ?>
                </div>
                
            </div><!-- end row-->
        </div>
        
    </main>

<?php get_footer(); ?>