<?php
/*******************************
 * Template: About Page
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>
<?php get_header(); ?>

<?php include("inc/hasform.php");?>
<?php include("inc/gsap.php");?>

<main class="homepage">
    <!--Homepage Hero Section-->
    <div class="about_hero">

        <div class="container about_content text-white">
            <div class="row pb">
                <div class="col-sm-6 align-self-start">
                    <h1 id="hero_heading" class="pb-4">Who we are?</h1>
                    <p id="hero_para">INFLYX is a European liquidity, risk management and white label solutions provider. <br><br>
                        Our main goal is to establish transparency when providing liquidity services and help our clients mitigate risk as effectively as possible. <br><br>
                        Our success is built on honesty, flexibility, diversity, experience and reasonable pricing structures.
                    </p>
                    <div class="py-5">
                        <a id="hero_cta" class="cta" href="<?php echo esc_url( site_url('/contact/')); ?>">Get in touch</a>
                    </div>
                </div>
            </div>

            <div class="row pt-5">
                <div class="col-12 text-center">
                    <div class="pt-5">
                        <img id="move_down" src="<?php echo get_theme_file_uri(); ?>/assets/img/down.webp" alt="Scroll Down">
                    </div>
                </div>

            </div>
        </div>

    </div>

    <!-- Separator -->
    <div class="container-fluid separator"></div>

    <!-- Homepage section -->
    <div class="container-fluid text-white">

        <div class="row">
            <div id="liq_provider" class="col-sm-8 bg_dark_blue py-5">
                <div class="an_inflyx py-4">
                    <h2>INFLYX Liquidity Provider</h2>
                    <p>Expand your options with incomparable market depth and access to a vast array of symbols</p>
                </div>
            </div>
            <div class="col-sm-4 bg_dark_green py-5 ">
                <h3 class="py-4 text-center"><a href="<?php echo esc_url( site_url('/liquidity/')); ?>">Liquidity Services</a></h3>
                <div class="text-center">
                    <a href="<?php echo esc_url( site_url('/liquidity/')); ?>">
                        <img id="white_arrow" class="ml-4 pl-5" src="<?php echo get_theme_file_uri(); ?>/assets/img/arr_white.webp" alt="Liquidity Services -->">
                    </a>
                </div>

            </div>
        </div>

    </div>
    
    <div class="container-fluid bg_home_city pt-5">
        <div class="container pt-5">
            <div class="row pt-5">
                <div class="col-12 col-md-7 homepage_offering_section p-5">
                    <h3>Comprehensive <br>Transparent Liquidity</h3>

                    <p class="pt-5">Sourcing liquidity and doing business with liquidity providers can at times be marked by a lack of openness.</p>
                    <p>This was the driving factor behind the foundation and establishment of INFLYX, a new, innovative liquidity provider that places emphasis on complete transparency of operations at all times.</p>
                    <p>Absolute transparency means that you, as a client, know what you can expect from us, have insight on how our pricing models are calculated and are aware of the steps we take to help you manage risk in the most comprehensive way possible.</p>

                    <!-- Separator -->
                    <div class="container-fluid separator green_gray"></div>

                </div>
                <div class="col-12 col-md-5 text-white">
                        <div class="about_icons pt-3">
                            <div class="column_fix">
                                <div class="img_cont">
                                    <img src="<?php echo get_theme_file_uri(); ?>/assets/img/execution.webp" alt="Leading Execution">
                                </div>
                                <h4>Leading <br> Execution</h4>
                            </div>
                            <div class="column_fix">
                                <div class="img_cont">
                                <img src="<?php echo get_theme_file_uri(); ?>/assets/img/complete-flexibility.webp" alt="Complete Flexibility">
                                </div>
                                <h4>Complete <br> Flexibility</h4>
                            </div>
                            <div class="column_fix">
                                <div class="img_cont">
                                <img src="<?php echo get_theme_file_uri(); ?>/assets/img/full-range.webp" alt="Ful Range of Markets">
                                </div>
                                <h4>Full Range <br> of Markets</h4>
                            </div>
                            <div class="column_fix">
                                <div class="img_cont">
                                <img src="<?php echo get_theme_file_uri(); ?>/assets/img/transparency.webp" alt="Absolute Transparency">
                                </div>
                                <h4>Absolute <br> Transparency</h4>
                            </div>
                        </div>

                </div>
            </div>
        </div>

    </div>
    <!-- Separator -->
    <div class="container-fluid separator2"></div>

    <!-- Why Inflyx -->
    <?php include("inc/why_inflyx.php"); ?>

    <!-- Separator -->
    <div class="container-fluid separator2"></div>

    <div class="container-fluid pt-5 bg_dark_green contact_sec">
        <div class="container homepage_talk pt-5">
            <div class="row pt-5">
                <div class="col-md-6 homepage_talk_to p-5">
                    <img alt="Inflyx" class="pt-5" src="<?php echo get_theme_file_uri(); ?>/assets/img/inflyx-logo.svg">
                    <h3 class="py-4">We would love to talk to you</h3>
                    <p>Feel free to contact us.</p>

                    <!-- Separator -->
                    <div class="container-fluid separator green_gray"></div>
                </div>

                <div class="col-md-6 text-white pl-5">
                    <?php include("inc/contactform.php"); ?>
                </div>
            </div>
        </div>

    </div>

    <!-- Separator -->
    <div class="container-fluid separator"></div>
    
    <!-- As Seen on -->
    <?php include("inc/as_seen_on.php"); ?>

</main>

<script>
    $(document).ready(function() {
        var $page_logo = $("#page_logo"),
            $hero_heading = $("#hero_heading"),
            $hero_para = $("#hero_para"),
            $hero_cta = $("#hero_cta"),
            $hero_menu = $(".menu-trigger"),
            $move_down = $("#move_down"),
            $liq_provider = $("#liq_provider"),
            $white_arrow = $("#white_arrow"),
            $blue_arrow = $("#blue_arrow"),
            $shows = $(".show");//selects every element with a class of show
        TweenLite.from($page_logo, 0.5, {scale:0.5, rotation: -10, ease:Back.easeOut, delay: 1, opacity: 0});
        TweenLite.from($hero_heading, 0.5, {x: -100, opacity:0, delay:2});
        TweenLite.from($hero_para, 1.5, {x: -50, opacity:0, delay: 2.5});
        TweenLite.from($hero_cta, 1.5, {opacity:0, delay:2.5});
        TweenLite.from($move_down, 1.5, {opacity: 0, delay:4.5});
        TweenLite.from($hero_menu, 1.1, {opacity:0, delay:0.3});
        var tl = new TimelineMax({ repeat: -1 , repeatDelay: 1})
            .to($move_down, 0.7, {ease: Back.easeOut.config(3), y: -40 });
        var tl2 = new TimelineMax({ repeat: -1 , repeatDelay: 2})
            .to($white_arrow, 0.7, {ease: Elastic.easeOut.config(0.5, 0.8), x: -40 });
        var tl3 = new TimelineMax({ repeat: -1 , repeatDelay: 2})
            .to($blue_arrow, 0.7, {ease: Elastic.easeOut.config(0.5, 0.8), x: -40 });
        $hero_menu.hover(function () {
            TweenLite.to(".line.line-3", 0.1, {css:{marginLeft: 0}﻿}﻿);
            TweenLite.to($hero_menu, 0.1, {scale: 1.2});
        });
        $hero_menu.mouseleave(function () {
            TweenLite.to(".line.line-3", 0.1, {css:{marginLeft: 11}﻿}﻿);
            TweenLite.to($hero_menu, 0.1, {scale: 1});
        });
        $liq_provider.hover(function () {
            TweenLite.to($liq_provider, 0.5, {css:{backgroundColor:"#033661"}﻿}﻿);
            TweenLite.to($liq_provider.find("h2"), 0.5, {scale: 1.1});
            TweenLite.to($liq_provider.find("p"), 0.5, {scale: 1.1});
        });
        $liq_provider.mouseleave(function () {
            TweenLite.to($liq_provider, 0.5, {css:{backgroundColor:"#00325C"}﻿}﻿);
            TweenLite.to($liq_provider.find("h2"), 0.5, {scale: 1});
            TweenLite.to($liq_provider.find("p"), 0.5, {scale: 1});
        });
        $move_down.click(function () {
            TweenLite.to(window, 1, {scrollTo:{y:$("#liq_provider").offset().top - 18}});
        })
        $( document ).on( "mousemove", function( event ) {
            if(event.pageY > 1350){
                TweenLite.to(".homepage_offering_section", 0.6, {opacity: 1});
                TweenLite.to(".show1", 0.6, {opacity: 1, delay: 0.5});
                TweenLite.to(".show2", 0.6, {opacity: 1, delay: 0.7});
                TweenLite.to(".show3", 0.6, {opacity: 1, delay: 0.9});
                TweenLite.to(".show4", 0.6, {opacity: 1, delay: 1.1});
                TweenLite.to(".show5", 0.6, {opacity: 1, delay: 1.3});
                TweenLite.to(".show6", 0.6, {opacity: 1, delay: 1.5});
                TweenLite.to(".show7", 0.6, {opacity: 1, delay: 1.7});
            }

        });
    });
</script>


<?php get_footer(); ?>