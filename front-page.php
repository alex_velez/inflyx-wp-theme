<?php
/*******************************
 * Template: Homepage
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>

<?php get_header(); ?>

    <?php include("inc/hasform.php");?>
    <?php include("inc/gsap.php");?>

    <main class="homepage">
        <!--Homepage Hero Section-->
        <div class="homepage_hero">
            <div class="homepage_hero_video"></div>

            <div class="container homepage_content text-white">
                <div class="row pb">
                    <div class="col-sm-6">
                        <h1 id="hero_heading">Bespoke Liquidity Solutions</h1>
                        <p id="hero_para">Partner with us and enjoy a Liquidity Provider that is Transparent, Diverse, Flexible and prides themselves of providing
                            Stellar Customer Service. * Expand your options with incomparable Market Depth & Access to a Vast Array of Symbols.</p>
                        <div class="py-5">
                            <a id="hero_cta" class="cta" href="<?php echo esc_url(site_url('/contact/')); ?>">Get in touch</a>
                        </div>
                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>

                <div class="row pt-5">
                    <div class="col-12 text-center">
                        <div class="pt-5">
                            <img id="move_down" src="<?php echo get_theme_file_uri(); ?>/assets/img/down.webp" alt="Scroll Down">
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- Separator -->
        <div class="container-fluid separator">

        </div>
           <!-- Homepage section -->
        <div class="container-fluid text-white">

            <div class="row">
                <div id="liq_provider" class="col-sm-8 bg_dark_blue py-5">
                    <div class="an_inflyx py-4">
                        <h2>INFLYX Liquidity Provider</h2>
                        <p>Expand your options with incomparable market depth and access to a vast array of symbols</p>
                    </div>
                </div>
                <div class="col-sm-4 bg_dark_green py-5 ">
                    <h3 class="py-4 text-center"><a href="<?php echo esc_url( site_url('/liquidity/')); ?>">Liquidity Services</a></h3>
                    <div class="text-center">
                        <a href="<?php echo esc_url( site_url('/liquidity/')); ?>">
                            <img id="white_arrow" class="ml-4 pl-5" src="<?php echo get_theme_file_uri(); ?>/assets/img/arr_white.webp" alt="Liquidity Services -->">
                        </a>
                    </div>

                </div>
            </div>
        </div>

        <div class="container-fluid bg_home_city pt-5">
            <div class="container pt-5">
                <div class="row pt-5">
                    <div class="col-md-6 homepage_offering_section p-lg-5 p-md-2">
                        <h3 class="offering_heading">
                           We are offering <br>
                            Multi-Asset Liquidity to:
                        </h3>
                        <ul class="list-dash">
                            <li class="show1"><span>Brokers/ White Labels</span></li>
                            <li class="show2"><span>Hedge Funds</span></li>
                            <li class="show3"><span>Investment Firms </span></li>
                            <li class="show4"><span>Asset Managers</span></li>
                            <li class="show5"><span>Professional Traders</span></li>
                            <li class="show6"><span>Proprietary Trading Houses</span></li>
                            <li class="show7"><span>Private Banks/ Corporates</span></li>
                        </ul>
                        <h3 class="text-right"><a class="_hlink" href="<?php echo esc_url( site_url('/liquidity/')); ?>">Liquidity Services</a></h3>
                        <div class="text-right">
                            <a href="<?php echo esc_url( site_url('/liquidity/')); ?>">
                                <img id="blue_arrow" class="ml-4 pl-5" src="<?php echo get_theme_file_uri(); ?>/assets/img/arr_blue.webp" alt="Liquidity Services --->">
                            </a>
                        </div>
                        <!-- Separator -->
                        <div class="container-fluid separator green_gray">

                        </div>
                    </div>
                    <div class="col-md-6 text-white p-5 next_step">
                        <h3>
                            Take the next step in your brokerage journey with <strong>INFLYX's</strong> tailored solutions
                        </h3>
                        <div class="py-5">
                            <a class="cta" href="<?php echo esc_url( site_url('/contact/')); ?>">Get in touch</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- Separator -->
        <div class="container-fluid separator2"></div>

        <div class="container-fluid bg_gray">
            <div class="container">
                <div class="row section_why sec_why_pad">
                    <div class="col-12 col-md-4 pb-5">
                        <h4>Why Inflyx?</h4>
                        <p>Because we can give you a competitive advantage over other businesses.</p>
                        <!-- Separator -->
                        <div class="container-fluid separator3 w-25"></div>
                    </div>
                    <div class="col-6 col-md-2">
                        <img class="inflyx_pros" src="<?php echo get_theme_file_uri(); ?>/assets/img/flexibility.webp" alt="Flexibility">
                        <h5>Flexibility</h5>
                    </div>
                    <div class="col-6 col-md-2">
                        <img class="inflyx_pros" src="<?php echo get_theme_file_uri(); ?>/assets/img/seamless-integration.webp" alt="Seamless Integration">
                        <h5>Seamless <br> Integration</h5>
                    </div>
                    <div class="col-6 col-md-2">
                        <img class="inflyx_pros" src="<?php echo get_theme_file_uri(); ?>/assets/img/interbank-spreads.webp" alt="Interbank Spreads">
                        <h5>Interbank <br> Spreads</h5>
                    </div>
                    <div class="col-6 col-md-2">
                        <img class="inflyx_pros" src="<?php echo get_theme_file_uri(); ?>/assets/img/dedicated-support.webp" alt="Dedicated Support">
                        <h5>Dedicated <br> Support</h5>
                    </div>
                </div>
            </div>
        </div>

        
        <!-- Separator -->
        <div class="container-fluid separator2"></div>

        <div class="container-fluid pt-5 bg_dark_green contact_sec">
            <div class="container homepage_talk pt-5">
                <div class="row pt-5">
                    <div class="col-md-6 homepage_talk_to p-5">
                        <img alt="Liquidity Solutions" class="pt-5" src="<?php echo get_theme_file_uri(); ?>/assets/img/inflyx-logo.svg">
                        <h3 class="py-4">We would love to talk to you</h3>
                        <p>Feel free to contact us.</p>
                        <!-- Separator -->
                        <div class="container-fluid separator green_gray"></div>
                    </div>
                    <div class="col-md-6 text-white pl-5">
                        <?php include("inc/contactform.php"); ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- Separator -->
        <div class="container-fluid separator"></div>

        <!-- Ass seen on section -->
        <?php include("inc/as_seen_on.php"); ?>

</main>

    <script>
        $(document).ready(function() {
            if($(window).width()>767) {
                $video = '<video playsinline muted loop poster="<?php echo get_theme_file_uri(); ?>/assets/img/heroScreenshot.webp" id="bgvid"> <source src="<?php echo get_theme_file_uri(); ?>/assets/video/heroVideo.mp4" type="video/mp4"></video>';
                $('.homepage_hero_video').html($video);
                $('#bgvid').get(0).play();
                $( document ).on( "mousemove", function( event ) {
                    if(event.pageY > 1350){
                        TweenLite.to(".homepage_offering_section", 0.4, {opacity: 1, delay:0.1});
                        TweenLite.to(".show1", 0.6, {opacity: 1, delay: 0.2});
                        TweenLite.to(".show2", 0.6, {opacity: 1, delay: 0.4});
                        TweenLite.to(".show3", 0.6, {opacity: 1, delay: 0.6});
                        TweenLite.to(".show4", 0.6, {opacity: 1, delay: 0.8});
                        TweenLite.to(".show5", 0.6, {opacity: 1, delay: 1.0});
                        TweenLite.to(".show6", 0.6, {opacity: 1, delay: 1.2});
                        TweenLite.to(".show7", 0.6, {opacity: 1, delay: 1.4});
                    }
                });
            }

            var $page_logo = $("#page_logo"),
                $hero_heading = $("#hero_heading"),
                $hero_para = $("#hero_para"),
                $hero_cta = $("#hero_cta"),
                $hero_menu = $(".menu-trigger"),
                $move_down = $("#move_down"),
                $homepage_hero = $(".homepage_hero"),
                $liq_provider = $("#liq_provider"),
                $white_arrow = $("#white_arrow"),
                $blue_arrow = $("#blue_arrow"),
                $shows = $(".show");//selects every element with a class of show
            TweenLite.from($homepage_hero, 1, {opacity: 0});
            TweenLite.from($page_logo, 0.5, {scale:0.5, rotation: -10, ease:Back.easeOut, delay: 1, opacity: 0});
            TweenLite.from($hero_heading, 0.5, {x: -100, opacity:0, delay:2});
            TweenLite.from($hero_para, 1.5, {x: -50, opacity:0, delay: 1.5});
            TweenLite.from($hero_cta, 1.5, {opacity:0, delay:1.5});
            TweenLite.from($move_down, 4.5, {opacity: 0, delay:2.5});
            TweenLite.from($hero_menu, 1.5, {opacity:0, delay:0.4});
            var tl = new TimelineMax({ repeat: -1 , repeatDelay: 1})
                .to($move_down, 1.7, {ease: Back.easeOut.config(3), y: -20 });
            var tl2 = new TimelineMax({ repeat: -1 , repeatDelay: 2})
                .to($white_arrow, 0.7, {ease: Elastic.easeOut.config(0.5, 0.8), x: -40 });
            var tl3 = new TimelineMax({ repeat: -1 , repeatDelay: 2})
                .to($blue_arrow, 0.7, {ease: Elastic.easeOut.config(0.5, 0.8), x: -40 });
            $hero_menu.hover(function () {
                TweenLite.to(".line.line-3", 0.1,{css:{marginLeft:0}﻿}﻿);
                TweenLite.to($hero_menu, 0.1, {scale: 1.2});
            });
            $hero_menu.mouseleave(function () {
                TweenLite.to(".line.line-3", 0.1, {css:{marginLeft: 11}﻿}﻿);
                TweenLite.to($hero_menu, 0.1, {scale: 1});
            });
            $liq_provider.hover(function () {
                TweenLite.to($liq_provider, 0.5, {css:{backgroundColor:"#033661"}﻿}﻿);
                TweenLite.to($liq_provider.find("h2"), 0.5, {scale: 1.1});
                TweenLite.to($liq_provider.find("p"), 0.5, {scale: 1.1});
            });
            $liq_provider.mouseleave(function () {
                TweenLite.to($liq_provider, 0.5, {css:{backgroundColor:"#00325C"}﻿}﻿);
                TweenLite.to($liq_provider.find("h2"), 0.5, {scale: 1});
                TweenLite.to($liq_provider.find("p"), 0.5, {scale: 1});
            });
            $move_down.click(function () {
                TweenLite.to(window, 1, {scrollTo:{y:$("#liq_provider").offset().top - 18}});
            })

        });
    </script>

<?php get_footer(); ?>