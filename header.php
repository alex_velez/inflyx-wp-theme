<?php
/*******************************
 * Template: Header
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>
 
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-148901858-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-148901858-1');
	</script>

    <!-- Inflyx Favicon -->
    <link rel="icon" href="<?php echo get_theme_file_uri(); ?>/assets/img/favicon.ico" sizes="32x32" />
    <link rel="icon" href="<?php echo get_theme_file_uri(); ?>/assets/img/favicon.ico" sizes="192x192" />
    <link rel="apple-touch-icon" href="<?php echo get_theme_file_uri(); ?>/assets/img/favicon.ico" />
    <meta name="msapplication-TileImage" content="<?php echo get_theme_file_uri(); ?>/assets/img/favicon.ico" />
    <script src="<?php echo get_theme_file_uri(); ?>/assets/js/jquery.js"></script>
    

    <!-- Page Title -->
    <?php if (is_front_page()) { ?>
        <title><?php echo bloginfo('name');?> | Bespoke Liquidity Solutions</title>
        <meta property="og:title" content="Inflyx | Bespoke Liquidity Solutions" />
        <meta property="og:description" content="How green is your liquidity solutions? Take the next step in your brokerage journey with INFLYX's tailored solutions." />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://inflyx.com/" />
        <meta property="og:image" content="https://inflyx.com/wp-content/uploads/2019/12/bespoke-liquidity-solutions.jpg" />
        <meta name="author" content="Inflyx">
    <?php } else { ?>
        <title><?php echo bloginfo('name');?> Bespoke Liquidity Solutions | <?php echo esc_html( get_the_title() ); ?></title> <?php
        } ?>
    <?php wp_head();?>
</head>

<body>

<?php include("inc/navigation.php"); ?>

<!-- Open n Close Nav script -->
<script>
    function openNav() {
        document.getElementById("myNav").style.width = "100%";
    }
    function closeNav() {
        document.getElementById("myNav").style.width = "0%";
    }
</script>