<?php
/*******************************
 * Template: Theme Scripts
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>

<!-- Jquery 3.4.1 -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.js"></script>

<!-- GSAP -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/gsap/TimelineMax.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/gsap/TweenMax.js"></script>
<script src="<?php echo get_template_directory_uri();?>/assets/js/inflyx.js"></script>


