<?php

/** Enqueue Theme Files **/
function inflyx_theme_files() {
    wp_enqueue_style( 'main-theme-style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'inflyx_theme_files' );


/** Additional Thumbnail Sizes **/
if ( function_exists( 'add_theme_support' ) ) { 
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'blog-thumb', 285, 190 );
}


add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes() {
    return 'class="page-link"';
}


function pagination_bar($max_pages) {

    $links = paginate_links( array(
        'prev_next'          => false,
        'type'               => 'array',
        'total'  => $max_pages
    ) );

    if ( $links ) :

        echo '<ul class="pagination text-center mx-auto">';

        // get_previous_posts_link will return a string or void if no link is set.
        if ( $prev_posts_link = get_previous_posts_link( __( '<i class="fas fa-angle-left"></i> Previous' ) ) ) :
            echo '<li class="prev-list-item">';
            echo $prev_posts_link;
            echo '</li>';
        endif;

        echo '<li class="page-item">';
        echo join( '</li><li class="page-item">', $links );
        echo '</li>';

        // get_next_posts_link will return a string or void if no link is set.

        if ( $next_posts_link = get_next_posts_link( __( 'Next <i class="fas fa-angle-right"></i>'), $max_pages ) ) :
            echo '<li class="next-list-item">';
            echo $next_posts_link;
            echo '</li>';
        endif;
        echo '</ul>';
    endif;
}
function wpdocs_custom_excerpt_length( $length ) {
    return 15;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );