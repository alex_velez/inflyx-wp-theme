<?php
/*******************************
 * Template: Liquidity Page
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>
<?php get_header(); ?>

<?php include("inc/hasform.php");?>
<?php include("inc/gsap.php");?>

<main class="homepage">
    <!--Homepage Hero Section-->
    <div class="liquidity_hero">
        <div class="container liquidity_content text-white">
            <div class="row pb">
                <div class="col-12 col-md-6 align-self-start">
                    <h1 id="hero_heading">Liquidity Services</h1>
                    <p id="hero_para">Expand your options with incomparable market depth and access to a vast array of symbols</p>
                </div>
            </div>
            <div class="row pt-5">
                <div class="col-md-6 d-none d-md-block">
                    <ul class="bullets">
                        <li id="bull_1a" class="bull_1a activelist"><span class="bull_1a">Brokers/White Labels</span></li>
                        <li id="bull_2a" class="bull_2a"><span class="bull_2a">Hedge Funds</span></li>
                        <li id="bull_3a" class="bull_3a"><span class="bull_3a">Investment Firms</span></li>
                        <li id="bull_4a" class="bull_4a"><span class="bull_4a">Asset Managers</span></li>
                        <li id="bull_5a" class="bull_5a"><span class="bull_5a">Professional Traders</span></li>
                        <li id="bull_6a" class="bull_6a"><span class="bull_6a">Proprietary Trading Houses</span></li>
                        <li id="bull_7a" class="bull_7a"><span class="bull_7a">Private Banks/ Corporates</span></li>
                    </ul>
                </div>
                <div class="col-md-6 bullets_content">
                    <h4 id="bull_1">Brokers / White Labels</h4>
                    <p class="mouse_scroll bull_1">
                        As a retail Brokerage we offer you a wide array of Raw Spreads, Competitive Trading Conditions,
                        Ultimate Market Depth, Prime Aggregation, Exceptional Execution, Lowest possible Swaps, and state of the art Technology.
                        Couple that with our stellar Customer Support and end-to-end service and what you have is the most complete solution on the market.
                        We are looking for long-term Partnerships and not short-term deals. We look forward to working with you.
                    </p>
                    <p class="mouse_scroll bull_1">
                        If you are looking to launch your White Label Brokerage, do not hesitate to contact us. We can offer you a full-scale solution that includes
                        a flexible CRM, a comprehensive Client Portal, IB Portal as well as website and Reporting tools to help launch your business and sustain
                        a successful and powerful position in the market.
                    </p>
                    <h4 id="bull_2">Hedge Funds</h4>
                    <p class="mouse_scroll bull_2">
                        Enjoy the lowest costs of Liquidity position by partnering with INFLYX. Due to our competitive Trading Conditions, exceptional Execution,
                        lowest possible Swaps, and bespoke Technology - your Strategies enjoy the best underlying asset that is bound to be reflected in the horizon
                        of your client's entire Portfolio.
                    </p>
                    <h4 id="bull_3">Investment Firms</h4>
                    <p class="mouse_scroll bull_3">
                        As an Institutional Investor Firm, you are able to invest in any of our Highly Liquid offerings, which include Forex, CFDs, Indices, Metals, and Stocks. Any volume can be covered by our Market Depth.
                    </p>
                    <h4 id="bull_4">Asset Managers</h4>
                    <p class="mouse_scroll bull_4">
                        Manage your client's Portfolios by gaining access to INFLYX Liquidity.  Due to our competitive Trading Conditions, Exceptional Execution, Lowest possible Swaps, and state of the art Technology, the Strategy you choose to embark in on behalf of your client, will enjoy the best underlying benefits the market has to offer.
                    </p>
                    <h4 id="bull_5">Professional Traders</h4>
                    <p class="mouse_scroll bull_5">
                        As a Professional Trader, Low Spreads, Fair Swaps and exceptional Execution are the working tools and foundation you need in your prosperous trading venture. Partner with us and enjoy the best the market has to offer.
                    </p>
                    <h4 id="bull_6">Proprietary Trading Houses</h4>
                    <p class="mouse_scroll bull_6">
                        Enjoy the best trading conditions for your speculative trades. Our Raw Spreads, Competitive Trading Conditions, Ultimate Market Depth, Prime Aggregation, exceptional Execution and lowest possible Swaps. Couple that with our stellar Customer Support and end-to-end service and what you have is the most complete solution on the market.
                    </p>
                    <h4 id="bull_7">Private Banks/Corporates</h4>
                    <p class="mouse_scroll bull_7">
                        Reduce exposure by using our Deep Liquidity or consider offering your clients the possibility to trade with real-time price updates.
                    </p>
                    <!-- <p style="visibility: hidden;">
                        This is the bottom of liquidity services
                    </p> -->
                </div>

            </div>

        </div>

    </div>

    <!-- Separator -->
    <div class="container-fluid separator2"></div>

    <div class="container-fluid bg_dark_green bg_linear py-5">
        <div class="row text-white text-center py-5">
            <div class="col-12 text-center d-md-none">
                <h5 class="pb-5">DISTRIBUTION OF LIQUIDITY</h5>
                <img class="img-fluid" src="<?php echo get_theme_file_uri(); ?>/assets/img/distribution_liq_mbl.svg" alt="Distribution of Liquidity">
            </div>
            <div class="col-12 text-center d-none d-md-block">
                <h5 class="pb-5">DISTRIBUTION OF LIQUIDITY</h5>
                <img class="img-fluid" src="<?php echo get_theme_file_uri(); ?>/assets/img/distribution_liq.svg" alt="Distribution of Liquidity">
            </div>
        </div>
    </div>
    <!-- Separator -->
    <div class="container-fluid separator"></div>

    <!-- Why Inflyx --> 
    <?php include("inc/why_inflyx.php"); ?>

    <!-- Separator -->
    <div class="container-fluid separator2"></div>


    <div class="container-fluid pt-5 bg_dark_green contact_sec">
        <div class="container homepage_talk pt-5">
            <div class="row pt-5">
                <div class="col-md-6 homepage_talk_to p-5">
                    <img alt="Inflyx" class="pt-5" src="<?php echo get_theme_file_uri(); ?>/assets/img/inflyx-logo.svg">
                    <h3 class="py-4">We would love to talk to you</h3>
                    <p>Feel free to contact us.</p>
                    <!-- Separator -->
                    <div class="container-fluid separator green_gray"></div>
                </div>

                <div class="col-md-6 text-white pl-5">
                    <?php include("inc/contactform.php"); ?>
                </div>
            </div>
        </div>

    </div>
    <!-- Separator -->
    <div class="container-fluid separator"></div>

    <!-- as seen on -->
    <?php include("inc/as_seen_on.php"); ?>

</main>

<script>
    $(document).ready(function() {

        $("ul.bullets li").click(function () {
            var current = "#" + event.target.classList[0].toString();

            var curr_content = current.substring(0, current.length-1);

            TweenLite.to(".bullets_content", 1, {scrollTo:{y: curr_content, offsetY:70, autoKill:false}});
            $("li").removeClass('activelist');
            $(current).addClass("activelist");
        })
        $(".mouse_scroll").hover(function () {
            var current_para = "#" + event.target.classList[1].toString() + "a";
            $("li").removeClass('activelist');
            $(current_para).addClass("activelist");
        });
        $(".mouse_scroll").on('touchstart touchend',function () {
            var current_para = "#" + event.target.classList[1].toString() + "a";
            $("li").removeClass('activelist');
            $(current_para).addClass("activelist");
        });
    });
</script>

<?php get_footer(); ?>
