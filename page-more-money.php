<?php
/*******************************
 * Template: More Money
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>

<?php get_header(); ?>

<script>
    $(function(){
        $(".moremoneyForm").submit(function(e){
            e.preventDefault();
            if($("#firstName").val().length == 0) {
                $("#name-error").text("Please enter your name");
                return false;
            }else if ($("#emailAddress").val().length == 0){
                $("#name-error").text("");
                $("#last-name-error").text("");
                $("#email-error").text("Please enter your email");
                return false;
            }else {
                var href = $(this).attr("action");
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: href,
                    data: $(this).serialize(),
                    success: function(response){
                        if(response.status == "success"){
                            window.location.href = "<?php echo esc_url(get_template_directory_uri() . '/assets/docs/liquidity-for-forex.pdf');?>";
                        }else{
                            alert("An error occured: " + response.message);
                        }
                    }
                });
            }
        });
    });
</script>

<main id="more-money">
    <!-- More Money Intro -->
    <div class="container-fluid intro">
        <div class="container">
            <div class="row justify-content-around">
                <div class="col-md-6">
                    <h1>How can I make my brokerage earn <span class="more">MORE MONEY?</span></h1>
                    <ul>
                        <li>- A choice of the most traded indices</li>
                        <li>- Competitive leverage</li>
                        <li>- Low commissions and more</li>
                    </ul>
                </div>
                <div class="col-12 col-md-5 text-white contact-form my-auto">
                    <h2>DOWNLOAD INFLYX PDF</h2>
                    <form name="contactForm" class="moremoneyForm" id="contactForm" action="https://formcarry.com/s/oOy2taY47l8" method="POST" accept-charset="UTF-8" >
                        <input type="text" name="fullName" id="firstName" placeholder="Full Name">
                        <p class="show-error" id="name-error"></p>
                        <input type="email" name="email" id="emailAddress" placeholder="Email">
                        <p class="show-error" id="email-error"></p>
                        <input type="text" name="phone" placeholder="Phone Number">
                        <input id="submitted" name="Submitted" type="hidden" value="on More Money">
                        <input type="hidden" name="_gotcha">
                        <input type="submit" value="FREE DOWNLOAD">
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>

<!-- Who we are section and Paul Orford Information -->
<div class="container-fluid" id="who-we-are">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 details my-auto">
                <h3>Who we are?</h3>
                <p>INFLYX is a European liquidity, risk management and white label solutions provider.</p>
                <p>Our main goal is to establish transparency when providing liquidity services and help our clients mitigate risk as effectively as possible.</p> 
                <p>Our success is built on honesty, flexibility, diversity, experience and reasonable pricing structures.</p>
            </div>
            <div class="col-12 col-md-6 d-none d-md-block">
                <img src="<?php echo esc_url(get_template_directory_uri() . '/assets/img/who-we-are-image.webp');?>" alt="Inflyx European liquidity" class="img-fluid">
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" id="paul-orford">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-12 col-md-6">
                <img src="<?php echo esc_url(get_template_directory_uri() . '/assets/img/paul-orford-alt.webp');?>" alt="Paul Orford Image" class="img-fluid">
            </div>
            <div class="col-12 col-md-5 my-auto">
                <div class="profile-information">
                    <div class="name">Paul Orford</div>
                    <div class="title">HEAD OF INSTITUTIONAL SALES</div>
                    <p>Operating in the financial markets for the past 15 years and a part of the institutional space for the last 6, Paul brings his expertise and unique blend of skills to our institutional sales department.</p>
                    <p>He is the owner of Game Changers magazine aimed at B2B FX and a regular contributor to Finance Feeds.</p>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Comprehensive Transparent Liquidity -->
<div class="container-fluid" id="icon-section">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-12 col-md-6 col-lg-5">
                <div class="information-box">
                    <div class="title">Comprehensive,<br>Transparent Liquidity</div>
                    <p>Sourcing liquidity and doing business with liquidity providers can at times be marked by a lack of openness. </p>
                    <p>This was the driving factor behind the foundation and establishment of INFLYX, a new, innovative liquidity provider that places emphasis on complete transparency of operations at all times.</p>
                    <p>Absolute transparency means that you, as a client, know what you can expect from us, have insight on how our pricing models are calculated and are aware of the steps we take to help you manage risk in the most comprehensive way possible.</p>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-5 text-white">
                <div class="about_icons pt-3">
                    <div class="column_fix">
                        <div class="img_cont">
                            <img src="<?php echo get_theme_file_uri(); ?>/assets/img/execution.webp" alt="Leading Execution">
                        </div>
                        <h4>Leading <br> Execution</h4>
                    </div>
                    <div class="column_fix">
                        <div class="img_cont">
                        <img src="<?php echo get_theme_file_uri(); ?>/assets/img/complete-flexibility.webp" alt="Complete Flexibility">
                        </div>
                        <h4>Complete <br> Flexibility</h4>
                    </div>
                    <div class="column_fix">
                        <div class="img_cont">
                        <img src="<?php echo get_theme_file_uri(); ?>/assets/img/full-range.webp" alt="Ful Range of Markets">
                        </div>
                        <h4>Full Range <br> of Markets</h4>
                    </div>
                    <div class="column_fix">
                        <div class="img_cont">
                        <img src="<?php echo get_theme_file_uri(); ?>/assets/img/transparency.webp" alt="Absolute Transparency">
                        </div>
                        <h4>Absolute <br> Transparency</h4>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>

<?php get_footer(); ?>
