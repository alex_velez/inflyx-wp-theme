<?php
/*******************************
 * Template: Single.php 
 * Axios Holding Team 
 * Inflyx Theme 2019
 *******************************/?>
<?php get_header(); ?>
<style>
    .logo_container {
        position:relative!important;
        top:0!important;
        padding:30px 0;
        background:rgba(10, 106, 121, 1);
    }
    .mb100 {
        margin-bottom:100px!important;
    }
</style>

<?php include("inc/hasform.php");?>  

<main class="contact">
    <div class="container-fluid">
        <div class="row">
            <!-- <div class="col-12 img-map-location"></div> -->
            <div class="mapouter">
                <div class="gmap_canvas">
                    <iframe width="600" height="500" id="gmap_canvas" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d820.2357295639317!2d32.99055382924145!3d34.681390101606716!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzTCsDQwJzUzLjAiTiAzMsKwNTknMjguMCJF!5e0!3m2!1sen!2sus!4v1575622572343!5m2!1sen!2sus" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                </div>
                    <style>
                        .mapouter{position:relative;text-align:right;height:500px;width:600px;}
                        .gmap_canvas {overflow:hidden;background:none!important;height:500px;width:600px;}
                    </style>
            </div>
        </div>
    </div>
    <!-- Separator -->
    <div class="container-fluid separator2"></div>

    <div class="container-fluid pt-5 bg_dark_green contact_sec">
        <div class="container homepage_talk pt-5">
            <div class="row justify-content-around pt-5">
                <div class="col-12 col-md-6 homepage_talk_to p-5">
                    <img alt="Inflyx" class="pt-5" src="<?php echo get_theme_file_uri(); ?>/assets/img/inflyx-logo.svg">
                    <h1 class="py-4" style="font-size:1.2em;">We would love to talk to you</h1>
                    <p>Feel free to contact us.</p>
                   
                    <!-- Separator -->
                    <div class="container-fluid separator green_gray"></div>
                    
                </div>

                <div class="col-12 col-md-5 text-white pl-5">
                    <?php include("inc/contactform.php"); ?>
                </div>
            </div>
        </div>

    </div>

    <!-- Separator -->
    <div class="container-fluid separator mb100"></div>

<?php get_footer(); ?>